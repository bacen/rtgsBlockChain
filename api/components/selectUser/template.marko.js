function create(__helpers) {
  var str = __helpers.s,
      empty = __helpers.e,
      notEmpty = __helpers.ne;

  return function render(data, out) {
    out.w('<div class="form-group"><label for="sel1">Select User</label><select class="form-control" id="userId" onchange="userChangeHandler(this.value)"></select></div><div class="form-group"><label for="sel1">Select Address</label><select class="form-control" id="globalKeystoreId" onchange="keyChangeHandler(this.value)"></select></div><script>\n\nvar globalUser;\nvar globalAddress;\nvar globalPassword;\n\nfunction userChangeHandler(user) {\n  console.log(\'user is now: \' + user);\n  globalUser = user;\n  \n  $.getJSON( "/users/" + user, function( data ) {\n      $(\'#globalKeystoreId\').empty();\n\t\t\t \n      $.each( data, function( key, val ) {\n          $(\'#globalKeystoreId\')\n              .append($(\'<option>\', { key : val })\r\n\t      .text(val));\r\n\t\t\t\t      \r\n      });\r\n\r\n      keyChangeHandler($(\'#globalKeystoreId\').val());\r\n  });\r\n}\r\n\r\nfunction keyChangeHandler(address) {\r\n  globalAddress = address;\r\n  $(\'#passwordModal\').modal(\'show\');\r\n  afterTX();\r\n}\r\n\r\nfunction loadHandler() {\r\n   $.getJSON( "/users/", function( data ) {\r\n      $.each( data, function( key, val ) {\r\n          console.log("iterating over data, key: " + key + " val: " + val);\r\n\r\n          $(\'#userId\')\r\n              .append($(\'<option>\', { key : val })\r\n\t      .text(val));\r\n\t\t\t \r\n      });\r\n\r\n      userChangeHandler(data[0]);\r\n   });\r\n}\r\n\r\n$( document ).ready( function () {\r\n    loadHandler();\r\n});\r\n\r\n</script>');
  };
}
(module.exports = require("marko").c(__filename)).c(create);