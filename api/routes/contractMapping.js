'use strict';

var express = require('express');
var helper = require('../lib/contract-helpers.js');
var router = express.Router();
var Promise = require('bluebird');
var Solidity = require('blockapps-js').Solidity;

var cors = require('cors');
var traverse = require('traverse');
var es = require('event-stream')

require('marko/node-require').install();

var homeTemplate = require('marko').load(require.resolve('../components/home/home.marko'));
var contractTemplate = require('marko').load(require.resolve('../components/contracts/template.marko'));

/* accept header used */

router.get('/:contractName/:contractAddress/state/mapping/:mappingName/:key', cors(), function (req, res) {

    var contractName = req.params.contractName;
    var contractAddress = req.params.contractAddress;
    var mappingName = req.params.mappingName;
    var key = req.params.key;

    var found = false;

    helper.contractsMetaAddressStream(contractName, contractAddress)
        .pipe(es.map(function (data, cb) {
            if (data.name == contractName) {
                found = true;
                cb(null, data);
            }
            else cb();
        }))

        .on('data', function (data) {
            var contract = Solidity.attach(data);
            return Promise.props(contract.state)
                .then(function (sVars) {


                    sVars[mappingName](key).then(function (data) {
                        res.send(data);
                    });

                })

                .catch(function (err) {
                    console.log("contract/state sVars - error: " + err)
                    res.send(JSON.stringify(err));
                });
        })

        .on('end', function () {
            if (!found) res.send("contract not found");
        });

});

router.get('/:contractName/:contractAddress/state/mapping/:mappingName/:key/mapping/:anotherMappingName/:anotherKey', cors(), function (req, res) {

    var contractName = req.params.contractName;
    var contractAddress = req.params.contractAddress;
    var mappingName = req.params.mappingName;
    var key = req.params.key;
    var anotherMappingName = req.params.anotherMappingName;
    var anotherKey = req.params.anotherKey;

    var found = false;

    helper.contractsMetaAddressStream(contractName, contractAddress)
        .pipe(es.map(function (data, cb) {
            if (data.name == contractName) {
                found = true;
                cb(null, data);
            }
            else cb();
        }))

        .on('data', function (data) {
            var contract = Solidity.attach(data);
            return Promise.props(contract.state)
                .then(function (sVars) {


                    sVars[mappingName](key).then(function (data) {
                        data[anotherMappingName](anotherKey).then(function(dt){
                            res.send(dt);
                        });
                    });

                })

                .catch(function (err) {
                    console.log("contract/state sVars - error: " + err)
                    res.send(JSON.stringify(err));
                });
        })

        .on('end', function () {
            if (!found) res.send("contract not found");
        });

});

module.exports = router;
