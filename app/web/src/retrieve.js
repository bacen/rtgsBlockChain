var Lightwallet = require('eth-lightwallet');
var Promise     = require('bluebird');
var Blockapps   = require('blockapps-js');
var Transaction = Blockapps.ethbase.Transaction;
var Solidity    = Blockapps.Solidity;
var ethValue    = Blockapps.ethbase.Units.ethValue;
var fs          = require('fs');
var SimpleStorage = require('../../../api/meta/SimpleStorage/Latest.json');

var read = Promise.promisify(fs.readFile);

var stringToStore = "Just a simple test of what I believe is a very long string to test blockapps api. It seems that some strings are being eaten by blockapps? Let's check!!!";

var address, privkey, storage;

read('../../../api/users/Banco Central/e3337e4934e2691a0f27d585025f59e112c49259.json', 'utf8')
.then(function(value) {
    var keystore = Lightwallet.keystore.deserialize(value);
    address = keystore.getAddresses()[0];
    privkey = keystore.exportPrivateKey(address, '123456789');
    return Solidity.attach(SimpleStorage);
})
.then(function(value) {
    storage = value;
    return storage.state.data;
})
.then(function(value) {
    console.log("String retrieved: " + value);
});
