salt.controller('CadastraTransacaoController', function ($scope, $state, StateService, $rootScope, TransactionService, $q) {
    var buscaIFs = function () {

        $scope.bancos = [];

        StateService.getFullState().then(function (data) {

            $scope.bancos = data.banksWeb;

        });

    }

    buscaIFs();

    $scope.cadastraTransacao = function () {

        var nomeBanco = _.find($scope.bancos, function (item) {
            return item.addrBank == $scope.bancoOrigemSelecionado;
        }).name;


        StateService.getMappingMappingState("banks",
            $scope.bancoOrigemSelecionado,
            "encryptedSharedSymKeys",
            $scope.bancoDestinoSelecionado).then(function (ret) {

                chaveSimetricaCripto = ret.encryptedTransactionSharedSymKey.replace(/\u0000/g, '');
                StateService.getMappingState("banks", $scope.bancoOrigemSelecionado).then(function (cert) {
                    certificadoOrigem = cert.certificate.replace(/\u0000/g, '');
                    $q.when(enveloped_DECRYPT(certificadoOrigem, $scope.chavePrivadaOrigem, chaveSimetricaCripto)).then(function (senha) {
                        console.log(senha);
                        //$scope.valor
                        valorCripto = CryptoJS.AES.encrypt($scope.valor, senha).toString();
                        TransactionService.executaTransacao(nomeBanco, $scope.bancoOrigemSelecionado, $scope.senhaCarteira, "createTransaction",
                            JSON.stringify({
                                "_rcptBank": $scope.bancoDestinoSelecionado,
                                "_value": $scope.valor,
                                "_transactionEncryptedData": valorCripto
                            })
                        ).then(function (ret) {
                            console.log(ret);
                        }, function (err) {
                            console.log(err);
                        });

                    });
                });

            });
    }
});

