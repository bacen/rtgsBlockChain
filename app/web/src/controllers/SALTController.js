salt.controller('SALTController', function ($scope, $state, StateService, $rootScope, TransactionService) {
    $rootScope.banco = {
        nomeCarteira: "",
        enderecoCarteira: "",
        senhaCarteira: ""
    };

    $scope.validar = function () {

        TransactionService.executaTransacao($rootScope.banco.nomeCarteira, $rootScope.banco.enderecoCarteira, $rootScope.banco.senhaCarteira, 'getMyInformation', '{}')
            .then(function (ret) {
                $rootScope.banco.nome = ret.data[0];
                //$rootScope.banco.saldo = parseInt(ret.data[1], 16);
                $state.go('/saldo');

            });
        StateService.getFullState().then(function (data) {
            $rootScope.ultimoIDTransacao = data.totalTransactions;
        });

    };

});
