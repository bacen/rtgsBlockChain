salt.controller('CadastraIFController', function ($scope, $state, StateService, $rootScope, TransactionService) {

    $scope.cadastraIF = function () {
        TransactionService.executaTransacao(nomeUsuarioBCB, enderecoBCB, $scope.senhaCarteiraBCB, "addBank",
            JSON.stringify({
                "_bank": $scope.enderecoCarteira,
                "_name": $scope.nomeIF,
                "_certificate": $scope.certificadoIF
            })
        ).then(function (ret) {
            $scope.enderecoCarteira = "";
            $scope.nomeIF = "";
            $scope.senhaCarteiraBCB = "";
            $scope.certificadoIF = "";
            console.log("Cadastrado com sucesso");
            buscaIFs();
        }, function (err) {
            console.log(err);
        });
    }

    var buscaIFs = function () {

        $scope.bancos = [];

        StateService.getFullState().then(function (data) {

            $scope.bancos = data.banksWeb;

        });

    }

    buscaIFs();

});