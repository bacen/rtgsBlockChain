salt.controller('ConsultaTransacoesController', function ($scope, $state, StateService, $rootScope, TransactionService, $q) {

    var buscaIFs = function () {

        $scope.bancos = [];

        StateService.getFullState().then(function (data) {
            $scope.bancos = data.banksWeb;
        });
    }
    buscaIFs();

    var buscaUltimosIDs = function () {
        var deferred = $q.defer();
        StateService.getFullState().then(function (state) {
            var ultimoID = parseInt(state.totalTransactions);
            deferred.resolve(_.remove(_.range(1, ultimoID + 1), function (x) { return x > 0 }));

        });
        return deferred.promise;
    }

    $scope.buscaUltimasTransacoes = function () {

        $scope.ultimasTransacoes = [];

        var ehBancoCentral = ($scope.bancoOrigemSelecionado == enderecoBCB);

        var funcaoBuscaIDs = ehBancoCentral ? buscaUltimosIDs() : StateService.getMappingState("bankTransactions", $scope.bancoOrigemSelecionado);

        funcaoBuscaIDs.then(function (ids) {
            //ids = _.takeRight(ids, 20);
            StateService.getMappingState("banks", $scope.bancoOrigemSelecionado).then(function (cert) {
                certificadoOrigem = cert.certificate.replace(/\u0000/g, '');
                var buscaUltimas = _.map(ids, function (id) {
                    return StateService.getMappingState("transactionLog", id).then(function (trans) {
                        //obj.id = id;
                        //ultimasTransacoes.push(obj);
                        if (ehBancoCentral) {
                            enderecoBancoOrigem = trans.senderBank;
                            enderecoBancoDestino = trans.rcptBank;

                        } else {
                            enderecoBancoOrigem = $scope.bancoOrigemSelecionado;
                            enderecoBancoDestino = $scope.bancoOrigemSelecionado == trans.rcptBank ? trans.senderBank : trans.rcptBank;
                        }

                        StateService.getMappingMappingState("banks",
                            enderecoBancoOrigem,
                            "encryptedSharedSymKeys",
                            enderecoBancoDestino).then(function (ret) {

                                if (ehBancoCentral) {
                                    chaveSimetricaCripto = ret.encryptedCentralBankTransactionSharedSymKey.replace(/\u0000/g, '');
                                } else {
                                    chaveSimetricaCripto = ret.encryptedTransactionSharedSymKey.replace(/\u0000/g, '');
                                }

                                //console.log(enderecoBancoDestino);
                                //console.log(ret.encryptedTransactionSharedSymKey);

                                $q.when(enveloped_DECRYPT(certificadoOrigem, $scope.chavePrivadaOrigem, chaveSimetricaCripto)).then(function (senha) {

                                    $scope.ultimasTransacoes.push({
                                        id: parseInt(id),
                                        enderecoBanco: trans.rcptBank,
                                        nomeBancoOrigem: _.find($scope.bancos, function (b) { return b.addrBank == trans.senderBank }).name,
                                        nomeBancoDestino: _.find($scope.bancos, function (b) { return b.addrBank == trans.rcptBank }).name,
                                        confirmada: trans.confirmed,
                                        valorDescriptografado: CryptoJS.AES.decrypt(trans.transactionEncryptedData.replace(/\u0000/g, ''), senha).toString(CryptoJS.enc.Utf8)
                                    });
                                    $scope.ultimasTransacoes = _.sortBy($scope.ultimasTransacoes, "id");
                                }, function (err) {
                                    console.log(err);
                                });

                            });
                    });
                });
                $q.all(buscaUltimas).then(function () {
                    console.log("concluido");

                });
            });


        });

    }

    $scope.confirmaTransacao = function (nomeBanco, id) {
        if ($scope.bancoOrigemSelecionado==enderecoBCB) nomeBanco = nomeUsuarioBCB;
        TransactionService.executaTransacao(nomeBanco,
            $scope.bancoOrigemSelecionado,
            $scope.senhaCarteira,
            "confirmTransaction",
            JSON.stringify({
                _transaction: id
            })).then(function (data) {
                $scope.buscaUltimasTransacoes();
                //$scope.valor = "";
                //$scope.senhaCarteira = "";
                //buscaUltimasTransacoes();
                //buscaSaldoAtual();
            })
    }

});