salt.controller('saldoController', function ($scope, $state, StateService, $rootScope, TransactionService, $q) {

    var buscaIFs = function () {

        $scope.bancos = [];

        StateService.getFullState().then(function (data) {

            $scope.bancos = data.banks;

        });

    }

    buscaIFs();

    var buscaUltimasTransacoes = function () {

        ultimasTransacoes = [];

        StateService.getMappingState("bankTransactions", $rootScope.banco.enderecoCarteira).then(function (ids) {

            var buscaUltimas = _.map(_.takeRight(ids, 10), function (id) {

                return StateService.getMappingState("transactionLog", id).then(function (obj) {
                    obj.id = id;
                    ultimasTransacoes.push(obj);
                });

            });

            $q.all(buscaUltimas).then(function () {
                console.log("concluido");
                $scope.ultimasTransacoes = _.sortBy(_.map(ultimasTransacoes, function (trans) {
                    return {
                        id: trans.id,
                        enderecoBanco: trans.rcptBank,
                        nomeBanco: _.find($scope.bancos, function (b) { return b.addrBank == trans.rcptBank }).name,
                        confirmada: trans.confirmed,
                        valor: parseInt(trans.value, 16)
                    };
                }), "id");
            });

        });

    };

    var buscaSaldoAtual = function(){
        StateService.getMappingState("balances", $rootScope.banco.enderecoCarteira).then(function (balance) {
            $scope.banco.saldo = parseInt(balance.relativeBalance, 16);
        });
    }

    $scope.transfere = function () {

        TransactionService.executaTransacao($rootScope.banco.nomeCarteira,
            $rootScope.banco.enderecoCarteira,
            $scope.senhaCarteira,
            "createTransaction",
            JSON.stringify({
                _rcptBank: $scope.bancoSelecionado,
                _value: $scope.valor
            })).then(function (data) {
                $scope.valor = "";
                $scope.senhaCarteira="";
                buscaUltimasTransacoes();
                buscaSaldoAtual();
            })

    };

    $scope.confirmaTransacao = function(id){
        TransactionService.executaTransacao($rootScope.banco.nomeCarteira,
            $rootScope.banco.enderecoCarteira,
            $scope.senhaCarteira,
            "confirmTransaction",
            JSON.stringify({
                _transaction: id
            })).then(function (data) {
                $scope.valor = "";
                $scope.senhaCarteira="";
                buscaUltimasTransacoes();
                buscaSaldoAtual();
            })
    }

    //TODO: Depende de buscaIFs
    buscaUltimasTransacoes();
    buscaSaldoAtual();

});